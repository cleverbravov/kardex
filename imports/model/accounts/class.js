import {Class,Validator,Enum} from 'meteor/jagi:astronomy';

//const collectionRoles = new Mongo.Collection("custom_roles", {idGeneration: 'MONGO'});
//Accounts.roles = collectionRoles
//Meteor.roles = Accounts.roles
const email = Class.create({
    name:'email',
    fields:{
            address:{type:String},
            verified:{type:Boolean}
        }
})

const profile = Class.create({
    name:'profile',
    fields:{
        firstName: {
            type: String,
        },
        LastName: {
            type: String,
        },
        cargo: {
            type: String,
        },
    }
})

export const users = Class.create({
    name:'users',
    collection:Meteor.users, 
    fields:{
        username: {
            type: String,
        },
        services: {
            type: Object,
        },
        emails: {
            type: [email],
            default:function(){
                return [new email];
            }            
        },
        profile:{
            type: profile,
            default:function(){
                return new profile;
            }   
        },
    },
})
export const roles = Class.create({
    name:'roles',
    collection:Meteor.roles,
    fields:{
        name:{
            type:String
        },
        description:{
            type:String,
        },
        createdAt:{
            type:Date,
            default: function() {
                return new Date();
            }
        }
    }
})

/*
let newrole = new roles()
    newrole.role = 'users',
    newrole.name = 'usuarios'
    newrole.description = 'rol para poder crear usuarios'
    newrole.save()
*/



