import { Template } from 'meteor/templating';
import '../../view/accounts/signup'
Template.signup.events({ 
    'submit form': function(event, template) {
    event.preventDefault();
    let form = $(event.currentTarget);
    form = form.serializeJSON();
    console.log(form)
    Meteor.call('newUser',form,function(err,res){
        if (err) {
            console.log(err)
            swal(
                'Error!',
                err.message,
                'error'
            )
        }
        if (!err) {
            swal(
                'Exito!',
                'Usuario creado correctamente!',
                'success'
            )
            $("form")[0].reset();
        }
    })
    return false

    }
})
