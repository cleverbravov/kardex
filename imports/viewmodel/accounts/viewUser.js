import { Template } from 'meteor/templating';
import '../../view/accounts/viewUser'
import { users } from '../../model/accounts/class';

Template.viewUser.onCreated(function(){
    let template = this
    template.autorun(function(){
            parameters = {
                filters:{},
                projection:{}
            }
            template.postSubscription = template.subscribe('viewUser', parameters);
    });
});
Template.viewUser.helpers({
    getusers: function () {
        let template = Template.instance()
        if(template.postSubscription.ready()){
            /*console.log(kardex.find({}).fetch())*/
            let resp = users.find({})
            console.log(resp.fetch())
            return resp
        }
    },
    /*'click .editUser': function(event,template){
        event.preventDefault();
        console.log(this)
        $('#myModalViewUser').modal('toggle')
    },*/
});
