import { Template } from 'meteor/templating';
import '../../view/accounts/login'


AccountsTemplates.configure({
  confirmPassword:false,
  /*reCaptcha: {
      siteKey: "6LeW86IUAAAAAE-sbvSZC9O9Ov28CfLgNksaPWNA-x", // aqui su propia clave de capcha y en el archivo
      theme: "light",
      data_type: "image"
  },
  showReCaptcha: true,*/
  forbidClientAccountCreation:true,
  texts: {
    button: {
      signUp: "Registrarse Ahora!",
      signIn: "Iniciar Sesion"
    },
    title: {
      signUp: "Registro de Usuarios",
      signIn: "Inicio de Sesion"
    },
  errors: {
      //captchaVerification: "Captcha verification failed!",
      loginForbidden: "Error al Iniciar Sesion, por favor intente nuevamente",
      
  }
  },
  onSubmitHook: ( error, state,info ) => {
      if ( !error && state === 'signIn' ) {          
          //FlowRouter.go('/home');
          window.location.replace("/home");
      }
  },
  onLogoutHook: ( error, state ) => {
      FlowRouter.go('/');
  },
});

var pwd = AccountsTemplates.removeField('password');
AccountsTemplates.removeField('email');

AccountsTemplates.addFields([{
       _id: "username",
       type: "text",
       displayName: "Usuario",
       placeholder: 'Usuario',
       required: true,
       minLength: 5,
       //re: /^[0-9]+$/,
       errStr: 'Usuario no valido',
   },
   {
       _id: 'email',
       type: 'email',
       required: true,
       
       re: /.+@(.+){2,}\.(.+){2,}/,
       errStr: 'Email no valido',
   },
   {
       _id: 'username_and_email',
       type: 'text',
       required: true,
       displayName: "Nombre de Usuario",
       placeholder: 'Nombre de Usuario',
   },
   {
       _id: 'password',
       type: 'password',
       displayName: "Contraseña",
       placeholder: 'Introduzca su contraseña',
       required: true,
       minLength: 6,
       errStr: 'Error contraseña debe tener al menos 6 letras',
   },
   //pwd
]);
